#include "kruncanite.h"

Kruncanite kruncanites[4];
Kruncanite femaleKruncanites[2];
char * kruncaniteQuotes[10];
int currentQuote;
int endOfRandom = 2;

void setup_kruncanites(void)
{
	jo_clear_screen();
	kruncaniteSprites[0] = jo_sprite_add_tga("TEX", "KRN.TGA", JO_COLOR_Transparent);
	kruncaniteSprites[1] = jo_sprite_add_tga("TEX", "KRN2.TGA", JO_COLOR_Transparent);
	kruncaniteSprites[2] = jo_sprite_add_tga("TEX", "KRNF1.TGA", JO_COLOR_Transparent);
	kruncaniteSprites[3] = jo_sprite_add_tga("TEX", "KRNF.TGA", JO_COLOR_Transparent);
	
	femaleKruncanites[0].position.rz = 30;
	femaleKruncanites[0].position.x = -JO_TV_WIDTH_2;
	femaleKruncanites[0].position.y = 30;
	femaleKruncanites[1].position.rz = -30;
	femaleKruncanites[1].position.x = JO_TV_WIDTH_2;
	femaleKruncanites[1].position.y = 30;

	kruncaniteQuotes[0] = "Gies a fag!";
	kruncaniteQuotes[1] = "A'm gaunae boot you!";
	kruncaniteQuotes[2] = "A'm gaunae hum you!";
	kruncaniteQuotes[3] = "A'm gaunae rip yer jaw!";
	kruncaniteQuotes[4] = "A'll boot ye!";
	kruncaniteQuotes[5] = "A'll crack ye!";
	kruncaniteQuotes[6] = "Ya bam, you!";
	kruncaniteQuotes[7] = "Want tae play fitba?!";
	kruncaniteQuotes[8] = "PINGAS!";
	// kruncaniteQuotes[0] = jo_sprite_add_tga("QUOTES", "QUOTE0.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[1] = jo_sprite_add_tga("QUOTES", "QUOTE1.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[2] = jo_sprite_add_tga("QUOTES", "QUOTE2.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[3] = jo_sprite_add_tga("QUOTES", "QUOTE3.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[4] = jo_sprite_add_tga("QUOTES", "QUOTE4.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[5] = jo_sprite_add_tga("QUOTES", "QUOTE5.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[6] = jo_sprite_add_tga("QUOTES", "QUOTE6.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[7] = jo_sprite_add_tga("QUOTES", "QUOTE7.TGA", JO_COLOR_Transparent);
	// kruncaniteQuotes[8] = jo_sprite_add_tga("QUOTES", "QUOTE8.TGA", JO_COLOR_Transparent);
}
void spawn_new_kruncanite(int id)
{
	kruncanites[id].position.x = jo_random(JO_TV_WIDTH) - JO_TV_WIDTH_2;
	//kruncanites[id].position.x = 0;
	kruncanites[id].timer = 0;

	if (jo_random(endOfRandom) == 3)
	{
		kruncanites[id].scale = 0.9;
		kruncanites[id].position.y = JO_TV_HEIGHT - 20;
	}
	else
	{
		kruncanites[id].scale = 1.5;
		kruncanites[id].position.y = JO_TV_HEIGHT;
	}
	
	kruncanites[id].cooler = 50 + jo_random(10);
	//animate_kruncanite(&kruncanite);
	kruncanites[id].currentFrame = kruncaniteSprites[0];
	animate_Kruncanite(id);
	kruncanites[id].currentState = 1;
	currentQuote = kruncaniteQuotes[jo_random(8)];
	jo_storyboard_play(kruncanites[id].spawnAnimation);
	display_speech_bubble(kruncanites[id].position.x, currentQuote);
}

void spawn_new_female_kruncanite(int id)
{
	femaleKruncanites[id].timer = 0;
	femaleKruncanites[id].cooler = 50 + jo_random(10);
	//animate_kruncanite(&kruncanite);
	femaleKruncanites[id].currentFrame = kruncaniteSprites[2];
	//animate_Kruncanite(id);
	femaleKruncanites[id].currentState = 1;
	currentQuote = kruncaniteQuotes[jo_random(8)];
	jo_storyboard_play(femaleKruncanites[id].spawnAnimation);
	display_speech_bubble(femaleKruncanites[id].position.x, currentQuote);
}

void animate_Kruncanite(int id)
{
	kruncanites[id].spawnAnimation = jo_storyboard_create_for_object(false, false, &kruncanites[id].position);
	jo_storyboard_create_translation_animation_using_direction(kruncanites[id].spawnAnimation, UP, 20, 10);
}

void destroy_all_kruncanites(void)
{
	
}


