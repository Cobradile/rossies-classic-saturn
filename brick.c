#include "brick.h"

Brick brick;

void throwBrick(int posX, jo_pos2D newTarget)
{
	if (brick.available)
	{
		brick.attributes.x = posX;
		//brick.attributes.x = jo_random() - 400;
		brick.attributes.y = JO_TV_HEIGHT_4 - 40;
		brick.attributes.z = 500;
		
		brick.target = newTarget;
		brick.velocity.x = ((brick.target.x - brick.attributes.x) / 70) * 1.5;
		brick.velocity.y = (JO_ABS(brick.velocity.x / 2)) + 1;
		brick.attributes.rz = jo_sin((brick.attributes.x - brick.target.x)/
			(brick.attributes.y + brick.target.y));
		brick.available = false;
	}
}

void draw_brick()
{
	if (!brick.available) 
	{
		slPushMatrix();
		jo_sprite_change_sprite_scale(1.1);
		jo_sprite_draw3D_and_rotate(brick.sprite, brick.attributes.x, brick.attributes.y, brick.attributes.z, brick.attributes.rz);
		slPopMatrix();
	}
}

