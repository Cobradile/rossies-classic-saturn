/*
** Forsaken Plane
** Matthew Suttinger & Emerald Nova (emeraldnovagames@gmail.com)
** 
** This work is licensed under a Attribution-NonCommercial 4.0 International License
** More info at: https://creativecommons.org/licenses/by-nc/4.0/legalcode
*/

#ifndef __AUDIO_H__
#define __AUDIO_H__

#include <jo/jo.h>
#include "timer.h"

#define MAX_SOUND_CHANNEL (8)

//	Jo Engine Sound Structs
typedef enum
{
    Mono8Bit = (0x00 | 0x10),
    Mono16Bit = (0x00 | 0x00),
    Stereo8Bit = (0x80 | 0x10),
    Stereo16Bit = (0x80 | 0x00)
}                   sound_mode;

typedef struct
{
    char            *data;
    unsigned int    data_length;
    sound_mode   	mode;
}                   sound;

//	SFX timer
FIXED audio_repeat_time;
FIXED audio_time[MAX_SOUND_CHANNEL];

//static sound     sfx1;
bool snd_loaded;
static sound sfx[3];

void load_pcm(const char * const filename, const sound_mode mode, sound *snd);
void menu_sfx_init(void);
void game_sfx_init(void);
void play_audio(int sound_index);
void play_audio_from_channel(int id, int to, int from);
void play_audio_on_channel(int id, int channel);
void sound_timer(void);

#endif
