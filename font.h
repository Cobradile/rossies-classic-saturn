/*
** Forsaken Plane
** Matthew Suttinger & Emerald Nova (emeraldnovagames@gmail.com)
** 
** This work is licensed under a Attribution-NonCommercial 4.0 International License
** More info at: https://creativecommons.org/licenses/by-nc/4.0/legalcode
*/

#ifndef __FONT_H__
#define __FONT_H__

#include <jo/jo.h>

//	Small Font
int font_small_tile_SID;
jo_tile     font_small_tile[40];

//	Functions
void font_init(void);
void bottom_center(int x, int y, int w, float scale, char * string);

#endif
