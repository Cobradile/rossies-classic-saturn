
#include "yunki.h"


/* Yunki States:
-1 = No shawin
0 = Deid / Dead
1 = Ordinar / Normal
2 = Stairtin attackin / Initiating attack phase
3 = Attackin(g)
*/
Yunki yunki;
int noMuives = 0;

/*
SCO: 'Is is juist tae set iverhing up
En: This is just to set up everything
*/
void yunkiSetup(void)
{
	pass = 0;
	yunki.frames[0] = jo_sprite_add_tga("TEX", "YUNKI1.TGA", JO_COLOR_Transparent);
	yunki.frames[1] = jo_sprite_add_tga("TEX", "YUNKI2.TGA", JO_COLOR_Transparent);
	yunki.position.x = -30;
	yunki.position.y = 0;
	yunki.health = 100;
	yunki.currentState = 1;
	yunki.currentFrame = 0;
	yunki.timer = 0;
	yunki.cooler = 100;
	yunki.scale = 1.05;
	yunkiSpawn();
}

//This spawns Yunki
void yunkiSpawn(void)
{
	display_speech_bubble(yunki.position.x, "Success, uhm... uhm...");
	yunki.currentFrame = yunki.frames[0];
	yunki.target.x = 0;
	yunki.target.y = 0;
	yunkiNewTarget();
}

void yunkiNewTarget(void)
{
	yunki.previousTarget = yunki.target;
	do {
		yunki.target.x = jo_random(JO_TV_WIDTH) - JO_TV_WIDTH_2;
		yunki.target.x = -yunki.target.x;
	} while (JO_ABS(yunki.previousTarget.x - yunki.target.x) < 150);
	do {
		yunki.target.y = jo_random(JO_TV_HEIGHT_4 - 20);
	} while (JO_ABS(yunki.previousTarget.y - yunki.target.y) < 10);
	yunki.velocity.x = (((yunki.target.x - yunki.position.x) / 30) + 1) * 2;
	yunki.velocity.y = (((yunki.target.y - yunki.position.y) / 30) + 1) * 2;
}

void yunkiNewTargetZero()
{
	yunki.target.x = -30;
	yunki.target.y = 0;
	yunki.velocity.x = (((yunki.target.x - yunki.position.x) / 30)) * 2;
	yunki.velocity.y = (((yunki.target.y - yunki.position.y) / 30)) * 2;
}

void yunkiLogic(void)
{
	if (yunki.currentState == 0 || yunki.currentState == 1)
	{
		yunki.position.x += yunki.velocity.x;
		yunki.position.y += yunki.velocity.y;
		yunki.scale = 0.8 + ((float)yunki.position.y / 100);

		if (JO_ABS(yunki.position.y - yunki.target.y) <= 5 || 
		JO_ABS(yunki.position.x - yunki.target.x) <= 10) 
		{
			noMuives += 1;
			if (noMuives < 5)
			{
				yunkiNewTarget();
			}
			else
			{
				yunkiNewTargetZero();
				noMuives = 0;
			}
		}
			
		if (JO_ABS(yunki.position.x) > JO_ABS(JO_TV_WIDTH) ||
			JO_ABS(yunki.position.y) > JO_ABS(JO_TV_HEIGHT) || yunki.position.y < 0) yunkiNewTargetZero();
	}
	else if (yunki.currentState == 2)
	{
		if (JO_ABS(yunki.position.x - yunki.target.x) > 10) yunki.position.x += yunki.velocity.x;
		if (JO_ABS(yunki.position.y - yunki.target.y) > 5) yunki.position.y += yunki.velocity.y;
		yunki.scale = 0.8 + ((float)yunki.position.y / 100);
		
		if (JO_ABS(yunki.position.y - yunki.target.y) <= 5 && 
		JO_ABS(yunki.position.x - yunki.target.x) <= 10)
		{
			yunki.timer = 0;
			yunki.currentState = 3;
			yunki.cooler = 70;
			yunki.currentFrame = yunki.frames[0];
		}
	}
	if (yunki.timer < yunki.cooler * 1.2)
	{
		yunki.timer += 1;
	}
	else if (yunki.currentState > 0)
	{
		yunki.timer = 0;
		if (yunki.currentState == 2)
		{
			yunki.currentState = 3;
			yunki.currentFrame = yunki.frames[0];
			yunki.position.x = yunki.target.x;
			yunki.position.y = yunki.target.y;
		}
		else
		{
			yunki.currentState = 1;
			yunki.position.x = yunki.target.x;
			yunki.position.y = yunki.target.y;
			yunkiNewTarget();
		}
	}
}

void yunkiHit(void)
{
	display_speech_bubble(yunki.position.x, "Success, ow... ow...");
	yunki.timer = 0;
	yunki.health -= 10;
	if (yunki.health > 0)
	{
		yunki.currentFrame = yunki.frames[1];
		yunkiNewTargetZero();
		yunki.currentState = 2;
	}
	else
	{
		yunki.currentState = 0;
		yunki.cooler = 500;
	}
}

