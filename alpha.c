#include "alpha.h"

Alpha alpha;

void animate_shoot(void)
{
	alpha.shootAnimation = jo_storyboard_create_for_object(false, true, &alpha.position);
	jo_storyboard_create_translation_animation_using_direction(alpha.shootAnimation, DOWN, 20, 10);
	jo_storyboard_create_translation_animation_using_direction(alpha.shootAnimation, UP, 10, 20);
	jo_storyboard_create_translation_animation_using_direction(alpha.shootAnimation, UP, 0, 20);
	jo_storyboard_create_translation_animation_using_direction(alpha.shootAnimation, DOWN, 25, 10);
	jo_storyboard_create_translation_animation_using_direction(alpha.shootAnimation, UP, 25, 10);
	//jo_storyboard_create_translation_animation(alpha.shootAnimation, 270, 1, 160
}

void animate_deid(void)
{
	alpha.deidAnimation = jo_storyboard_create_for_object(false, false, &alpha.position);
	jo_storyboard_create_translation_animation_using_direction(alpha.deidAnimation, DOWN, 10, 10);
}

void setup_alpha(void)
{
	alpha.ID = jo_sprite_add_tga("TEX", "ALPHAGUN.TGA", JO_COLOR_Transparent);
	animate_shoot();
	animate_deid();
	//load_sounds();
}

void start_alpha(void)
{
	alpha.currentState = 1;
	alpha.position.y = JO_TV_HEIGHT_2 - 30;
	alpha.scale = 0.45;
	alpha.currentState = 1;
	alpha.shootCooler = 70;
	alpha.health = 100;
}

void draw_alpha(void)
{
	slPopMatrix();
	slPushMatrix();
	jo_sprite_change_sprite_scale(alpha.scale);
	jo_sprite_draw3D(alpha.ID, alpha.position.x, alpha.position.y, 500);
	slPopMatrix();
}

// void load_sounds(void)
// {
	// load_pcm("SHOTGUN.PCM", JoSoundMono16Bit, &shotgunfx);
// }

void alphaDeath()
{
	alpha.currentState = 0;
	jo_storyboard_play(alpha.deidAnimation);
}


