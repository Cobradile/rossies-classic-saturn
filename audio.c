/*
** Forsaken Plane
** Matthew Suttinger & Emerald Nova (emeraldnovagames@gmail.com)
** 
** This work is licensed under a Attribution-NonCommercial 4.0 International License
** More info at: https://creativecommons.org/licenses/by-nc/4.0/legalcode
*/

#include "audio.h"

//	Stolen from Jo Engine implmentation (Audio.c)
static PCM             __PCM_Entries[MAX_SOUND_CHANNEL] =
{
    {(_Mono | _PCM8Bit), 0, 127, 0, 0x0, 0, 0, 0, 0},
    {(_Mono | _PCM8Bit), 1, 127, 0, 0x0, 0, 0, 0, 0},
    {(_Mono | _PCM8Bit), 2, 127, 0, 0x0, 0, 0, 0, 0},
    {(_Mono | _PCM8Bit), 3, 127, 0, 0x0, 0, 0, 0, 0},
    {(_Mono | _PCM8Bit), 4, 127, 0, 0x0, 0, 0, 0, 0},
    {(_Mono | _PCM8Bit), 5, 127, 0, 0x0, 0, 0, 0, 0},
	{(_Mono | _PCM8Bit), 6, 127, 0, 0x0, 0, 0, 0, 0},
	{(_Mono | _PCM8Bit), 7, 127, 0, 0x0, 0, 0, 0, 0},
};

//	SFX timer
FIXED audio_repeat_time = toFIXED(0.25);
int currentChannel = 0;
bool snd_loaded = false;

//	Taken from Jo Engine
void load_pcm(const char * const filename, const sound_mode mode, sound *snd)
{
    char            *pcm;
    int             len;

	pcm = jo_fs_read_file(filename, &len);
	snd_loaded = pcm;
    if (pcm == JO_NULL)
	{
        return;
	}
    snd->mode = mode;
    snd->data_length = len;
    snd->data = pcm;
}

void menu_sfx_init(void)
{	
	//&sfx.clear();
	load_pcm("SLIDE.PCM", Mono8Bit, &sfx[0]);
}

void game_sfx_init(void)
{
	/*
		Sound Effects Initialization
		Loads sound effectsi nto memory
	*/
	//&sfx.clear();
	//load_pcm("BRUH.PCM", Mono8Bit, &sfx1);
	load_pcm("SHOTGUN.PCM", Mono8Bit, &sfx[0]);
	load_pcm("BOOM.PCM", Mono8Bit, &sfx[1]);
	load_pcm("BRUH.PCM", Mono8Bit, &sfx[2]);
}

// Searches for a free channel and then plays the audio_repeat_time
// WARNING: Using this too often will result in a crash
void play_audio(int sound_index)
{
	/*
		Play Audio
	*/
	
	/*
	do
	{
		currentChannel += 1;
		if (currentChannel >= MAX_SOUND_CHANNEL)
		{
			currentChannel = 0;
		}
	} while (audio_time[currentChannel] + audio_repeat_time > time);
	*/
	
	//	Find oldest channel
	currentChannel = -1;
	FIXED oldest_time = time;
	for(int i = 0; i < MAX_SOUND_CHANNEL; i++)
	{
		//	Overwrite oldest time if current channel's time is older
		if(audio_time[i] < oldest_time)
		{
			// Logic can be added to fail by not allowing audio time + timer to exceed time
			currentChannel = i;
			oldest_time = audio_time[i];
		}
	}
		
	//	If channel is found, play
	if((currentChannel >= 0) && (currentChannel < MAX_SOUND_CHANNEL))
	{
		slPCMOff(&__PCM_Entries[currentChannel]);
		slPCMOn(&__PCM_Entries[currentChannel],
			sfx[sound_index].data, sfx[sound_index].data_length);
		audio_time[currentChannel] = time;
	}
}

void play_audio_on_channel(int i, int ch)
{
	/*
		Play Audio
		Plays audio files if corresponding trigger is activated and repeat
		timer is exceeded
	*/
	if(audio_time[ch] + audio_repeat_time <= time)
	{
		slPCMOff(&__PCM_Entries[ch]);
		slPCMOn(&__PCM_Entries[ch], sfx[i].data, sfx[i].data_length);
		audio_time[ch] = time;
	}
}