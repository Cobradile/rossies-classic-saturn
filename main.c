/*
** Jo Sega Saturn Engine
** Copyright (c) 2012-2017, Johannes Fetz (johannesfetz@gmail.com)
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Johannes Fetz nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL Johannes Fetz BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <jo/jo.h>
#include "global.h"
#include "menu.h"
#include "game.h"
#include "font.h"

static int gameMode = 0;

void main_loop(void)
{
	/*
	Main Loop
	
	Called back every cycle
	*/
	jo_sprite_restore_sprite_scale();
	switch(gameMode)
	{
		case 0:
			jo_clear_screen();
			draw_intro();
			break;
		default:
			update();
			my_draw();
			break;
	}
}

void changeGameMode(int newMode)
{
	gameMode = newMode;
	switch(gameMode)
	{
		case 0:
			menu_sfx_init();
			start_intro();
			break;
		case 1:
			game_sfx_init();
			load_gemm();
			break;
		case 2:
			menu_sfx_init();
			loadEndSlides();
			gameMode = 0;
			break;
	}
}

void jo_main(void)
{
	jo_core_init(JO_COLOR_Black);
	
	//	Initialization of resources
	font_init();
	jo_audio_play_cd_track(2, 2, 1);
	changeGameMode(0);

	//	Callback
	jo_core_add_callback(main_loop);

	jo_core_run();
}

/*
** END OF FILE
*/
