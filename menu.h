#ifndef __MENU_H__
# define __MENU_H__

#include <jo/jo.h>
#include "game.h"
#include "audio.h"
#include "font.h"
#include "global.h"

void setupSlides(void);
void draw_intro(void);
void activate_slides(void);
void load_next_scene(void);
void startMenu(void);
void loadEndSlides(void);

char * slideTexts[14];

#endif /* !__MENU_H__ */
