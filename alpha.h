#ifndef __ALPHA_H__
# define __ALPHA_H__

#include <jo/jo.h>
#include "audio.h"

typedef struct {
	jo_pos2D position;
	int ID;
	int currentState; // 0 = Deid, 1 = Alive, 2 = Shuit
	int shootTimer;
	int shootCooler;
	int health;
	float scale;
	
	jo_storyboard *shootAnimation;
	jo_storyboard *deidAnimation;
} Alpha;

extern Alpha alpha;

void setup_alpha(void);
void animate_shoot(void);
void animate_deid(void);
void draw_alpha(void);
void alphaDeath();
// void controlPlayer(int level);

#endif /* !__ALPHA_H__ */
