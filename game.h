#ifndef __GAME_H__
# define __GAME_H__
	
#include <jo/jo.h>
#include "audio.h"
#include "alpha.h"
#include "kruncanite.h"
#include "yunki.h"
#include "brick.h"
#include "speechbubble.h"
#include "font.h"
#include "global.h"
	
static inline FIXED isColliding(int pos1, int size1, int pos2, int size2);
static inline FIXED isColliding2D(jo_pos2D pos1, int size1, jo_2d_object_attributes pos2, int size2);
void activateBoss(void);
void brickLogic(void);
void controlPlayer();
void update(void);
void my_draw(void);
void stairtGemm(void);
void load_backgrund(void);
void load_gemm(void);

#endif /* !__GAME_H__ */


