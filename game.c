#include "game.h"

typedef struct {
	jo_2d_object_attributes attributes;
	jo_2d_object_attributes velocity;
	bool showing;
	float timer;
} Slime;

Slime slime[5];
Slime drop[20];
int slimeAmoont = 3;
int currentSlime = 0;
int drapAmoont = 10;

int slimeSprite;
int dropSprite;

float slimeCooler = 50;

int kruncaniteCount = 0;
int level = 1;
int maxFKruncanites = 0;

bool pause = false;
int pauseTimer = 0;

bool loading = false;
int loadTimer = 30;

/*
Game Mode Flag

0 = Video Intros
1 = Start Screen
2 = Prologue Slides
3 = Epilogue Slides
*/
bool isAtBoss = false;

FIXED oldTime = toFIXED(0.25);

static inline FIXED isColliding(int pos1, int size1, int pos2, int size2)
{
    if (pos1 - size1 < pos2 + size2 && pos1 + size1 > pos2 - size2) 
	{
		return true;
	}
	return false;
}

static inline FIXED isColliding2D(jo_pos2D pos1, int size1, jo_2d_object_attributes pos2, int size2)
{
    if (pos1.x - size1 < pos2.x + size2 && pos1.x + size1 > pos2.x - size2 &&
		pos1.y - size1 < pos2.y + size2 && pos1.y + size1 > pos2.y - size2) 
	{
		return true;
	}
	return false;
}

void activateBoss(void)
{
	int c;
	for (c = 0; c < 5; c++)
	{
		explodeKruncanite(c);
	}
	for (c = 0; c < 2; c++)
	{
		explodeFKruncanite(c);
	}
	brick.available = true;
	jo_sprite_free_from(kruncaniteSprites[0]);
	brick.sprite = jo_sprite_add_tga("TEX", "SMOKE.TGA", JO_COLOR_Transparent);
	yunkiSetup();
}

void checkCollision(int id)
{
	if (isColliding(alpha.position.x + 20, 10, kruncanites[id].position.x, 45))
	{
		play_audio(1);
		//play_audio(3);
		explodeKruncanite(id);
		if (kruncaniteCount % 30 == 0)
		{
			level += 1;
			if (level == 3)
			{
				maxFKruncanites = 1;
			}
			else if (level >= 4)
			{
				maxFKruncanites = 2;
			}
		}
		else if (kruncaniteCount >= 100)
		{
			loading = true;
			isAtBoss = true;
		}
	}
}

void checkFCollision(int id)
{
	if (isColliding(alpha.position.x + 20, 10, femaleKruncanites[id].position.x, 45))
	{
		play_audio(1);
		//play_audio(3);
		explodeFKruncanite(id);
		if (kruncaniteCount % 33 == 0)
		{
			level += 1;
			switch (level)
			{
				case 2:
					maxFKruncanites = 1;
					break;
				case 3:
					endOfRandom = 3;
					break;
				case 4:
					maxFKruncanites = 2;
					break;
			}
		}
	}
}

void brickLogic(void)
{
	brick.attributes.x += brick.velocity.x;
	brick.attributes.y += brick.velocity.y;
	
	//brick.attributes.rz = jo_sin((brick.attributes.x - alpha.position.x)/
	//		(brick.attributes.y + alpha.position.y));
	
	if (brick.attributes.y >= JO_TV_HEIGHT_2 + 50) brick.available = true;
	
	if (isColliding2D(alpha.position, 15, brick.attributes, 10))
	{
		alpha.health -= 10;
		brick.available = true;
		if (alpha.health <= 0) alphaDeath();
	}
}

void controlPlayer()
{	
	if (alpha.currentState > 0)
	{
		if (alpha.currentState == 1)
		{
			if (jo_is_pad1_key_pressed(JO_KEY_A))
			{
				alpha.currentState = 2;
				//play_audio(1);
				//alpha.position.y = JO_TV_HEIGHT_2 - 30;
				jo_storyboard_play(alpha.shootAnimation);
				play_audio(0);
				if (!isAtBoss)
				{
					int c;
					for (c = 0; c < level; c++)
					{
						if (kruncanites[c].currentState > 0)
						checkCollision(c);
					}
					for (c = 0; c < maxFKruncanites; c++)
					{
						if (femaleKruncanites[c].currentState > 0)
						checkFCollision(c);
					}
				}
				else if (isColliding(alpha.position.x + 20, 10, yunki.position.x, 75))
				{
					if (yunki.currentState == 1)
					{
						yunkiHit();
						play_audio(1);
						activateSlime(yunki.position);
					}
				}
				
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_C) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				//activateBoss();
				isAtBoss = true;
				loading = true;
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_DOWN) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				alpha.health = 10;
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_UP) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				alpha.health = 10000;
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_X) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				if (isAtBoss) yunki.health -= 10;
				else kruncaniteCount += 10;
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_Z) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				if (isAtBoss) yunki.health += 10;
				else kruncaniteCount -= 10;
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_Y) && jo_is_pad1_key_pressed(JO_KEY_L) && jo_is_pad1_key_pressed(JO_KEY_R))
			{
				int c;
				for (c = 0; c < level; c++)
				{
					explodeKruncanite(c);
				}
				for (c = 0; c < 2; c++)
				{
					explodeFKruncanite(c);
				}
			}
		}
		
		if (jo_is_pad1_key_pressed(JO_KEY_LEFT) && alpha.position.x > -(JO_TV_WIDTH_2))
		{
			alpha.position.x -= 10;
		}
		if (jo_is_pad1_key_pressed(JO_KEY_RIGHT) && alpha.position.x < (JO_TV_WIDTH_2 - 20))
		{
			alpha.position.x += 10;
		}
	}
	else if (jo_is_pad1_key_pressed(JO_KEY_A))
	{
		loading = true;
		// changeGameMode(0);
		//changeGameMode(2);
	}
	if (jo_is_pad1_key_pressed(JO_KEY_B))
	{
		alpha.currentState = 0;
		loading = true;
	}
}

void explodeKruncanite(int id)
{
	kruncanites[id].currentState = 0;
	kruncanites[id].timer = 0;
	activateSlime(kruncanites[id].position);
	kruncaniteCount += 1;
}

void explodeFKruncanite(int id)
{
	femaleKruncanites[id].currentState = 0;
	femaleKruncanites[id].timer = 0;
	activateSlime(femaleKruncanites[id].position);
	kruncaniteCount += 1;
}

void kruncaniteLogic(int id)
{
	if (kruncanites[id].timer >= kruncanites[id].cooler)
	{
		switch (kruncanites[id].currentState)
		{
			case -1:
				kruncanites[id].currentState = 0;
			case 0:
				spawn_new_kruncanite(id);
				kruncanites[id].timer = 0;
				break;
			case 1:
				if (brick.available)
				{
					kruncanites[id].currentState = 2;
					kruncanites[id].timer = 40;
				}
				break;
			case 2:
				switch (kruncanites[id].currentFrame)
				{
					case 44:
						kruncanites[id].currentFrame = kruncaniteSprites[1];
						kruncanites[id].timer = 40;
						break;
					case 45:
						kruncanites[id].currentFrame = kruncaniteSprites[0];
						kruncanites[id].currentState = 1;
						throwBrick(kruncanites[id].position.x, alpha.position);
						kruncanites[id].timer = -100;
						break;
				}
				break;
		}
	}
	else
	{
		kruncanites[id].timer += 1;
	}
}

void femaleKruncaniteLogic(int id)
{
	if (femaleKruncanites[id].timer >= femaleKruncanites[id].cooler)
	{
		switch (femaleKruncanites[id].currentState)
		{
			case -1:
				femaleKruncanites[id].currentState = 0;
			case 0:
				spawn_new_female_kruncanite(id);
				femaleKruncanites[id].timer = 0;
				break;
			case 1:
				if (brick.available)
				{
					femaleKruncanites[id].currentState = 2;
					femaleKruncanites[id].timer = 40;
				}
				break;
			case 2:
				switch (femaleKruncanites[id].currentFrame)
				{
					case 46:
						femaleKruncanites[id].currentFrame = kruncaniteSprites[3];
						femaleKruncanites[id].timer = 40;
						break;
					case 47:
						femaleKruncanites[id].currentFrame = kruncaniteSprites[2];
						femaleKruncanites[id].currentState = 1;
						throwBrick(femaleKruncanites[id].position.x, alpha.position);
						femaleKruncanites[id].timer = -100;
						break;
				}
				break;
		}
	}
	else
	{
		femaleKruncanites[id].timer += 1;
	}
}

void update(void)
{
	if (!loading)
	{
		if (!pause)
		{
			if (jo_is_pad1_key_pressed(JO_MOUSE_START_BUTTON))
			{
				if (pauseTimer <= 0 && !loading) 
				{
					pause = true;
					pauseTimer = 10;
				}
			}
			if (pauseTimer > 0) pauseTimer--;
			
			controlPlayer();
			//sound_timer();
			timer();
			
			if (!isAtBoss)
			{
				int c;
				for (c = 0; c < level; c++)
				{
					kruncaniteLogic(c);
				}
				for (c = 0; c < maxFKruncanites; c++)
				{
					femaleKruncaniteLogic(c);
				}
			}
			else
			{
				if (yunki.currentState > 0)
				{
					yunkiLogic();
					if (yunki.currentState == 3)
					{
						if (yunki.timer >= yunki.cooler)
						{
							if (pass < 3)
							{
								if (brick.available)
								{
									throwBrick(yunki.position.x, alpha.position);
									pass += 1;
								}
								yunki.timer = 0;
							}
							else
							{
								pass = 0;
								yunki.timer = 0;
								yunki.currentState = 1;
							}
						}
						else
						{
							yunki.timer += 1;
						}
					}
				}
				else
				{
					yunkiLogic();
					if (yunki.timer >= yunki.cooler)
					{
						if (yunki.currentState >= 0)
						{
							yunki.currentState = -1;
							jo_sprite_free_from(alpha.ID);
							changeGameMode(2);
						}
					}
					else if (yunki.timer % 10 == 0)
					{
						int c;
						for (c = 0; c < slimeAmoont; c++)
						{
							if (!slime[c].showing)
							{
								play_audio(1);
								activateSlime(yunki.position);
								if (drapAmoont < 20) drapAmoont += 1;
								if (slimeAmoont < 5) slimeAmoont += 1;
								break;
							}
						}
					}
				}
			}
			if (!brick.available) brickLogic();
		}
		else if (jo_is_pad1_key_pressed(JO_MOUSE_START_BUTTON))
		{
			if (pauseTimer <= 0)
			{
				pause = false;
				pauseTimer = 5;
			}
			else pauseTimer--;
		}
	}
	if (alpha.currentState == 2)
	{
		if (alpha.shootTimer > alpha.shootCooler)
		{
			jo_storyboard_suspend(alpha.shootAnimation);
			alpha.shootTimer = 0;
			alpha.currentState = 1;	
			//alpha.position.y = JO_TV_HEIGHT_2 - 30;
		}
		else
		{
			alpha.shootTimer++;
		}
	}
}

void my_draw(void)
{
	if (loading)
	{
		jo_clear_screen();
		if (loadTimer <= 0)
		{
			if (isAtBoss) 
			{
				if (alpha.currentState > 0) activateBoss();
				else
				{
					isAtBoss = false;
					jo_sprite_free_from(yunki.frames[0]);
					setup_kruncanites();
					brick.sprite = jo_sprite_add_tga("TEX", "BRICK.TGA", JO_COLOR_Transparent);
					stairtGemm();
				}
			}
			else stairtGemm();
			loadTimer = 30;
			loading = false;
		}
		else
		{
			if (language == 0) bottom_center(JO_TV_WIDTH_2 - 49, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Loadin...");
			else bottom_center(JO_TV_WIDTH_2 - 50, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Loading...");
			loadTimer--;
		}
	}
	else
	{
		int c;
		
		if (pause)
		{
			if (language == 0) bottom_center(JO_TV_WIDTH_2 - 20, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Pausit");
			else bottom_center(JO_TV_WIDTH_2 - 20, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Paused");
		}
		else if (alpha.currentState == 0)
		{
			if (language == 0) jo_printf(1, 1, "Ye'r deid! Push A or B tae conteena.");
			else jo_printf(1, 1, "You're dead! Press A or B to continue.");
		}
		else if (!isAtBoss)
		{
			if (language == 0) jo_printf(1, 1, "Alpha Halth: %d  Kruncanite Coont: %d ", alpha.health, kruncaniteCount);
			else jo_printf(1, 1, "Alpha Health: %d Kruncanite Count: %d ", alpha.health, kruncaniteCount);
		}
		else
		{
			if (language == 0) jo_printf(1, 1, "Alpha Halth: %d    Yunki's Halth: %d    ", alpha.health, yunki.health);
			else jo_printf(1, 1, "Alpha Health: %d  Yunki's Health: %d    ", alpha.health, yunki.health);
		}
		
		//jo_list_foreach(&kruncanites, drawKruncanite);
		if (!isAtBoss)
		{
			slPushMatrix();
			for (c = 0; c < level; c++)
			{	
				if (kruncanites[c].currentState > 0)
				{
					jo_sprite_change_sprite_scale(kruncanites[c].scale);
					jo_sprite_draw3D(kruncanites[c].currentFrame, kruncanites[c].position.x, kruncanites[c].position.y, 500);
				}
			}
			jo_sprite_change_sprite_scale(1.5);
			for (c = 0; c < maxFKruncanites; c++)
			{	
				if (femaleKruncanites[c].currentState > 0)
				{
					jo_sprite_draw3D_and_rotate(femaleKruncanites[c].currentFrame, femaleKruncanites[c].position.x, femaleKruncanites[c].position.y, 500, femaleKruncanites[c].position.rz);
					//jo_sprite_draw3D(femaleKruncanites[c].currentFrame, femaleKruncanites[c].position.x, femaleKruncanites[c].position.y, 500);
				}
			}
		}
		else
		{
			if (yunki.currentState >= 0)
			{
				slPushMatrix();
				jo_sprite_change_sprite_scale(yunki.scale);
				jo_sprite_draw3D(yunki.currentFrame, yunki.position.x, yunki.position.y, 500);
			}
		}
		drawSlime();
		if (speechBubble.showing)
		{
			if (speechBubble.timer < speechBubble.cooler)
			{
				jo_sprite_change_sprite_scale(1.6);
				
				if (speechBubble.position.x < speechBubble.origX)
				{
					jo_sprite_enable_horizontal_flip();
				}
				jo_sprite_draw3D(speechBubble.sprite, speechBubble.position.x, speechBubble.position.y, 500);
				jo_sprite_disable_horizontal_flip();
				jo_sprite_change_sprite_scale(1);
				//jo_printf(0, 5, "Test: %d  ", speechBubble.position.y);
				//jo_sprite_draw3D(currentQuote, speechBubble.position.x, speechBubble.position.y, 500);
				bottom_center(speechBubble.position.x + 130, 40, speechBubble.position.x + 200, 1, speechBubble.message);
				//jo_font_print_centered(speechBubble.position.x, speechBubble.position.y, JO_COLOR_Black, kruncaniteQuotes);
				speechBubble.timer += 1;
			}
		}
	}
	
	draw_alpha();
	draw_brick();
}

void drawSlime(void)
{
	int c;
	for (c = 0; c < slimeAmoont; c++)
	{
		if (slime[c].showing)
		{
			if (slime[c].timer <= 0)
			{
				slime[c].timer = 0;
				slime[c].showing = false;
			}
			else
			{
				slime[c].timer -= 5;
				jo_sprite_change_sprite_scale((slimeCooler - slime[c].timer) / 50);
				jo_sprite_draw3D(slimeSprite, slime[c].attributes.x, slime[c].attributes.y, 500);
			}
		}
	}
	
	for (c = 0; c < drapAmoont; c++)
	{
		if (drop[c].showing)
		{
			if (drop[c].attributes.y >= JO_TV_HEIGHT)
			{
				drop[c].showing = false;
			}
			else
			{
				jo_sprite_change_sprite_scale(1.05);
				drop[c].attributes.x += drop[c].velocity.x;
				drop[c].attributes.y += drop[c].velocity.y;
				drop[c].attributes.rz += drop[c].velocity.rz;
				drop[c].velocity.y += 1;
				jo_sprite_draw3D_and_rotate(dropSprite, drop[c].attributes.x, drop[c].attributes.y, drop[c].attributes.z, drop[c].attributes.rz);
			}
		}
	}
	
	jo_sprite_restore_sprite_scale();
}

void activateSlime(jo_pos2D newPos)
{
	slime[currentSlime].showing = true;
	slime[currentSlime].attributes.x = newPos.x;
	slime[currentSlime].attributes.y = newPos.y;
	slime[currentSlime].timer = slimeCooler;
	currentSlime += 1;
	if (currentSlime >= slimeAmoont) currentSlime = 0;

	int c;
	for (c = 0; c < drapAmoont; c++)
	{
		if (!drop[c].showing)
		{
			drop[c].timer = slimeCooler;
			drop[c].attributes.x = newPos.x;
			drop[c].attributes.y = newPos.y;
			drop[c].velocity.y = -jo_random(5);
			drop[c].velocity.x = jo_random(20) - 10;
			drop[c].attributes.z = 500;
			drop[c].velocity.rz = jo_random(10) - 5;
			drop[c].showing = true;
		}
	}
}

void stairtGemm(void)
{
	yunki.currentState = -1;
	level = 1;
	
	brick.available = true;
	kruncaniteCount = 0;
	start_alpha();
	
	int c;
	for (c = 0; c < level; c++)
	{
		kruncanites[c].currentState = -1;
	}
	
	//spawn_new_kruncanite();
}

void load_backgrund(void)
{
	int c;
	jo_img bg;
	bg.data = JO_NULL;
	jo_tga_loader(&bg, "BG", "COLLEGE.TGA", JO_COLOR_Transparent);
	jo_set_background_sprite(&bg, 0, 0);
	jo_zoom_background(0.75);
	for (c = 0; c < 10; c++)  jo_draw_background_line(3, 3 + c, 235, 3 + c, JO_COLOR_DarkGray);
	jo_free_img(&bg);
	
	// jo_img bg[6];
	// bg[0].data = JO_NULL;
	// bg[1].data = JO_NULL;
	// bg[2].data = JO_NULL;
	// bg[3].data = JO_NULL;
	// bg[4].data = JO_NULL;
	// bg[5].data = JO_NULL;
	// jo_clear_background(JO_COLOR_DarkGray);
	// jo_tga_loader(&bg[0], "BG", "PORCH.TGA", JO_COLOR_Transparent);
	// jo_tga_loader(&bg[1], "BG", "TREE.TGA", JO_COLOR_Transparent);
	// jo_tga_loader(&bg[2], "BG", "PAVEMENT.TGA", JO_COLOR_Transparent);
	// jo_tga_loader(&bg[3], "BG", "ROADART.TGA", JO_COLOR_Transparent);
	// jo_tga_loader(&bg[4], "BG", "BRICKS.TGA", JO_COLOR_Transparent);
	// jo_tga_loader(&bg[5], "BG", "WINDAE.TGA", JO_COLOR_Transparent);
	
	// int c, f;
	// for (f = 0; f < 90; f += 16)
	// {
		// for (c = 0; c < JO_TV_WIDTH; c += 24)
		// {
			// jo_set_background_sprite(&bg[4], c, f);
		// }
	// }
	
	// for (f = 0; f < 90; f += 30)
	// {
		// for (c = 0; c < JO_TV_WIDTH; c += 30)
		// {
			// jo_set_background_sprite(&bg[5], c, f);
		// }
	// }
	
	// jo_set_background_sprite(&bg[0], 125, 59);
	// jo_set_background_sprite(&bg[1], 30, 70);
	// jo_set_background_sprite(&bg[1], 59, 70);
	// jo_set_background_sprite(&bg[1], 90, 70);
	// jo_set_background_sprite(&bg[1], 210, 70);
	// jo_set_background_sprite(&bg[1], 240, 70);
	// jo_set_background_sprite(&bg[1], 270, 70);
	// jo_set_background_sprite(&bg[2], 0, 180);
	// jo_set_background_sprite(&bg[3], 220, 145);
	// jo_free_img(&bg[0]);
	// jo_free_img(&bg[1]);
	// jo_free_img(&bg[2]);
	// jo_free_img(&bg[3]);
	// jo_free_img(&bg[4]);
	// jo_free_img(&bg[5]);
}

void load_gemm()
{
	loading = true;
	load_backgrund();
	setup_alpha();
	setup_speech_bubble();
	slimeSprite = jo_sprite_add_tga("TEX", "SLIME.TGA", JO_COLOR_Transparent);
	dropSprite = jo_sprite_add_tga("TEX", "SLIDROP.TGA", JO_COLOR_Transparent);
	setup_kruncanites();
	brick.sprite = jo_sprite_add_tga("TEX", "BRICK.TGA", JO_COLOR_Transparent);
	
	stairtGemm();
}



