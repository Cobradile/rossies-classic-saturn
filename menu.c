/*
** Jo Sega Saturn Engine
** Copyright (c) 2012-2017, Johannes Fetz (johannesfetz@gmail.com)
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Johannes Fetz nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL Johannes Fetz BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "menu.h"

typedef struct {
	int slide;
	char * message;
} InfoSlide;

InfoSlide infoSlide[13];

int currentSlide = 0;

int introLogo = 0;

int slides [10];
int mode = 0;
int nextMode = 2;
int endSlide = 11;

int currentOption = 0;

int frame = 0;

int buttonTimer = 0;
const int BUTTON_COOLER = 10;


void setupSlides(void)
{
	if (language == 0)
	{
		slideTexts[0] = "Guid Evenin, Comrade. This is the general. We'll be firin you intae Covanmill, Glesgae fur 'is        mission.";
		slideTexts[1] = "Thare's an abandoned college      thare, which tirns oot tae no be  'at abandoned.";
		slideTexts[2] = "The college is aye pure hoachin wi NEDs, but leukin closer, thay'r no yer ordinar NEDs at  aw. Thay'r   itherwarldy, even.   These yins ur tot identical, an mair come oot ae the college'n git in, an ur       gittin bigger in worryin nummers.";	
		slideTexts[3] = "Whan thay war caucht hivin a      batterin, insteid ae bluid, black slime came oot. We teuk a saumple  back tae base, an fun ot 'at 'e   slime isna fae here. Nane of it's chemical components ur like       onythin we'v iver seen.";
		slideTexts[4] = "The source ae the bluiid wis      traced back tae a recent diskivert planet cawed \"Kruncia\". The planet is awfu polutit an fou ae         cigarette smeuk. The ootlins ur    adaptit tae bide thare, an canna  survive unner Yird's cleaner      condeetions, sae thay uise fags as breathin masks.";
		slideTexts[5] = "Ither oddities kythed the mair we dug intae it. The leid thay tawk  soond an awfu lot like Weegee fae a distance, but wis actual a hale seperate leid, which we ur cawin  Kruncrarian.";
		slideTexts[6] = "It tirns oot 'at the Kruncanites  hiv a clonin faceelity unnerneath the college, which is the raison  load ae 'aim hiv kythed in sic a  short time, an we'v fund oot 'at  thay are plannin tae tak ower the  warld.";
		slideTexts[7] = "...an 'is is the mastermind ahint it aw:      Emperor Yunki!";
		slideTexts[8] = "Accordin'ae documents stolen fae  thair HQ, he's plannin tae alter  the slime fur the 2020s, an it    will transform ordinary fowk even  quicker, an wance he's got        iverywan unner his control, he's  gaun'ae tirn the planet intae a   giant fag, and mak iverywan ficht tae the death tae smeuk hit.";
		slideTexts[9] = "The survivor will become the new  Emperor. We must stap this fae    iver happenin, an malafouster     Emperor Yunki!";
		slideTexts[10] = "We'v haed a few ettlins tae       malafouster the biggin an fyle    thair plans, bit iverywan haes    been capturt an either killed or  turnt intae a Kruncanite. 'At's   how we'r sending yersel Alpha Ross Ye're wir last howp.";
		slideTexts[11] = "Ye'v been equipped wi a dart      shotgun, tae take them out. 100   Kruncanites shoud be eneuch tae   attract Emperor Yunki. A'v gien ye aw the information ye'll be       needin. Good luck, comrade! Ye'll be needin it.";
		slideTexts[12] = "efter a lang an haurd ficht,      Emperor Yunki dee'd... ae cancer";
		slideTexts[13] = "The warld is saufd wance mair,    the few Kruncanites left hiv been  roondit up and deportit back tae  Kruncia. Good job comrade!";
	}
	else
	{
		slideTexts[0] = "Good Evening, Comrade. This is the general. We will be sending you to Covanmill, Glasgow for this       mission.";
		slideTexts[1] = "There is an abandoned college     there, which turns out not to be  so abandoned.";
		slideTexts[2] = "The college is always packed with NEDs, but upon closer inspection, they're not your typical NEDs at  all. They're otherworldy, even.   These ones are completely         identical, and more come out of   the college than go in, and are   increasing in worrying numbers.";	
		slideTexts[3] = "When they were caught having a    fierce fight, instead of blood,   black slime came out. We took a    sample back to base, and found out that the slime is not of this     world. None of it's chemical       components are like anything seen here.";
		slideTexts[4] = "The source of the blood was traced back to a recently discovered     planet known as \"Kruncia\". The    planet is extremely poluted and   full of cigarette smoke. The      aliens are adapted to live there, and can't survive under Earth's   relatively clean conditions, so   they use cigarettes as breathing   masks.";
		slideTexts[5] = "Other oddities appeared the more  we dug into it. The language they speak sound like Glaswegian Scots from a distance, but was actually an entirely seperate language,    which we are calling Kruncrarian.";
		slideTexts[6] = "It turns out that the Kruncanites have a cloning facility underneath the college, which is the reason  so many have appeared in such a   short time, and we have founds out that they are planning to take    over the world.";
		slideTexts[7] = "...and this is the mastermind     behind it all:      Emperor Yunki!";
		slideTexts[8] = "According to documents stolen from their HQ, he's planning to genetically alter the slime for the     2020s, and it will transform      people even quicker, and one he's got everybody under his control,  he's going to turn the planet intoa giant cigarette, and make       everyone fight to the death to    smoke it.";
		slideTexts[9] = "The survivor will become the new  Emperor. We must stop this from   ever taking place, and destroy    Emperor Yunki!";
		slideTexts[10] = "We've had a few attempts to       destroy the building and foil their    plans, but everyone has been captured and either killed or     turned into a Kruncanite. That's  why we're sending you, Alpha Ross        You're our last hope.";
		slideTexts[11] = "You've been equipped with a dart  shotgun, to take them out. 100    Kruncanites should be enough to   attract Emperor Yunki. I've given you all the information you'll    need. Good luck, comrade! You'll  need it.";
		slideTexts[12] = "After a long and hard fight,      Emperor Yunki died... of cancer";
		slideTexts[13] = "The world is safe once more, the  few Kruncanites left have been     rounded up and deported back to   Kruncia. Good job comrade!";
	}
}

void activate_slides(void)
{
	setupSlides();
	jo_clear_background(JO_COLOR_Black);
	currentSlide = 0;
	slides[0] = jo_sprite_add_tga("SLIDES", "SLIDE1.TGA", JO_COLOR_Transparent);
	slides[1] = jo_sprite_add_tga("SLIDES", "SLIDE2.TGA", JO_COLOR_Transparent);
	slides[2] = jo_sprite_add_tga("SLIDES", "SLIDE3.TGA", JO_COLOR_Transparent);
	slides[3] = jo_sprite_add_tga("SLIDES", "SLIDE4.TGA", JO_COLOR_Transparent);
	slides[4] = jo_sprite_add_tga("SLIDES", "SLIDE5.TGA", JO_COLOR_Transparent);
	slides[5] = jo_sprite_add_tga("SLIDES", "SLIDEVI.TGA", JO_COLOR_Transparent);
	slides[6] = jo_sprite_add_tga("SLIDES", "SLIDE7.TGA", JO_COLOR_Transparent);
	slides[7] = jo_sprite_add_tga("SLIDES", "SLIDE8.TGA", JO_COLOR_Transparent);
	
	infoSlide[0].message = slideTexts[0];
	infoSlide[0].slide = 0;
	
	infoSlide[1].message = slideTexts[1];
	infoSlide[1].slide = 1;
	
	infoSlide[2].message = slideTexts[2];
	infoSlide[2].slide = 1;
	
	infoSlide[3].message = slideTexts[3];
	infoSlide[3].slide = 2;
	
	infoSlide[4].message = slideTexts[4];
	infoSlide[4].slide = 3;
	
	infoSlide[5].message = slideTexts[5];
	infoSlide[5].slide = 3;
	
	infoSlide[6].message = slideTexts[6];
	infoSlide[6].slide = 4;
	
	infoSlide[7].message = slideTexts[7];
	infoSlide[7].slide = 5;
	
	infoSlide[8].message = slideTexts[8];
	infoSlide[8].slide = 6;
	
	infoSlide[9].message = slideTexts[9];
	infoSlide[9].slide = 6;
	
	infoSlide[10].message = slideTexts[10];
	infoSlide[10].slide = 7;
	
	infoSlide[11].message = slideTexts[11];
	infoSlide[11].slide = 7;
	
	endSlide = 11;
	
	mode = 2;
}

void loadEndSlides(void)
{
	jo_clear_background(JO_COLOR_Black);
	mode = -1;
	nextMode = 3;
}

void displayEndSlides(void)
{
	slides[0] = jo_sprite_add_tga("slideTexts", "SLIDE9.TGA", JO_COLOR_Transparent);
	infoSlide[0].slide = 0;
	infoSlide[0].message = slideTexts[12];
	
	slides[1] = jo_sprite_add_tga("slideTexts", "SLIDE10.TGA", JO_COLOR_Transparent);
	infoSlide[1].slide = 1;
	infoSlide[1].message = slideTexts[13];
	
	currentSlide = 0;
	endSlide = 1;
	
	mode = 3;
}

void load_next_scene(void)
{
	// jo_core_remove_callback(draw_intro);
	mode = 4;
	changeGameMode(1);
}

void start_intro(void)
{
	introLogo = jo_sprite_add_tga("INTRO", "LOGO.TGA", JO_COLOR_Transparent);
	mode = 0;
}

void draw_intro(void)
{
	/*
	Draw Introduction
	
	
	*/

	/*
	Game Mode Flag

	-1 = Loading
	0 = Video Intros		(Not Implemented)
	1 = Start Screen
	2 = Prologue Slides
	3 = Epilogue Slides		(Not Implemented)
	4 = Game
	5 = End Screen
	*/

	if (mode == -1)
	{
		if (frame >= 30)
		{
			mode = nextMode;
			switch(nextMode)
			{
				case 1:
					jo_sprite_free_all();
					font_init();
					mode = 1;
					changeGameMode(0);
					break;
				case 2:
					activate_slides();
					break;
				case 3:
					displayEndSlides();
					break;
				case 4:
					jo_sprite_free_from(slides[0]);
					load_next_scene();
					break;
				case 5:
					jo_sprite_free_from(slides[0]);
					break;
			}
			frame = 0;
		}
		else
		{
			if (currentOption == 0) bottom_center(JO_TV_WIDTH_2 - 49, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Loadin...");
			else bottom_center(JO_TV_WIDTH_2 - 50, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Loading...");
			frame++;
		}
	}
	else if (mode == 0)
	{
		if (currentOption == 0)
		{
			jo_sprite_restore_sprite_scale();
			bottom_center(JO_TV_WIDTH_2 - 35, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH - 5, 0.1, "Scots");
			jo_sprite_change_sprite_scale(0.8);
			bottom_center(JO_TV_WIDTH_2 - 41, JO_TV_HEIGHT_2, JO_TV_WIDTH - 5, 0.1, "English");
			if (jo_is_pad1_key_pressed(JO_KEY_DOWN)) currentOption++;
		}
		else if (currentOption == 1)
		{
			jo_sprite_change_sprite_scale(0.8);
			bottom_center(JO_TV_WIDTH_2 - 35, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH - 5, 0.1, "Scots");
			jo_sprite_restore_sprite_scale();
			bottom_center(JO_TV_WIDTH_2 - 41, JO_TV_HEIGHT_2, JO_TV_WIDTH - 5, 0.1, "English");
			if (jo_is_pad1_key_pressed(JO_KEY_UP)) currentOption--;
		}
		
		if (jo_is_pad1_key_pressed(JO_KEY_A))
		{
			language = currentOption;
			mode = 1;
		}
	}
	else if (mode == 1)
	{
		//	Start Screen
		jo_sprite_draw3D(introLogo, 0, -JO_TV_HEIGHT_4, 500);
		if (language == 0)
		{
			bottom_center(70, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Programmed bi Cobra!");
			bottom_center(59, JO_TV_HEIGHT_2 + 10, JO_TV_WIDTH, 1, "Wi help fae Emerald Nova");
			bottom_center(45, JO_TV_HEIGHT_2 + 30, JO_TV_WIDTH, 1, "Muisic an Concept Airt bi Q");
			bottom_center(70, JO_TV_HEIGHT_2 + 50, JO_TV_WIDTH, 1, "Based on a concept bi         General McKenzie");
			bottom_center(JO_TV_WIDTH_2 - 50, JO_TV_HEIGHT - 30, JO_TV_WIDTH, 1, "Push Stairt");
		}
		else
		{
			bottom_center(70, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "Programmed by Cobra!");
			bottom_center(40, JO_TV_HEIGHT_2 + 10, JO_TV_WIDTH, 1, "With help from Emerald Nova");
			bottom_center(47, JO_TV_HEIGHT_2 + 30, JO_TV_WIDTH, 1, "Music and Concept Art by Q");
			bottom_center(70, JO_TV_HEIGHT_2 + 50, JO_TV_WIDTH, 1, "Based on a concept by         General McKenzie");
			bottom_center(JO_TV_WIDTH_2 - 50, JO_TV_HEIGHT - 30, JO_TV_WIDTH, 1, "Press Start");
		}
		if (jo_is_pad1_key_pressed(JO_MOUSE_START_BUTTON))
		{
			jo_sprite_free_from(introLogo);
			mode = -1;
			nextMode = 2; 
		}
	}
	if (mode == 5)
	{
		bottom_center(JO_TV_WIDTH_2 - 40, JO_TV_HEIGHT_2 - 10, JO_TV_WIDTH, 1, "The end!");
		if (language == 0) bottom_center(JO_TV_WIDTH_2 - 89, JO_TV_HEIGHT_2, JO_TV_WIDTH, 1, "Cheers fur Playin!");
		else bottom_center(JO_TV_WIDTH_2 - 90, JO_TV_HEIGHT_2, JO_TV_WIDTH, 1, "Thanks for Playing!");
	}
	else if (mode >= 2)
	{
		//	Prologue
		timer();
		jo_sprite_restore_sprite_scale();
		bottom_center(5, JO_TV_HEIGHT_2 + 10, JO_TV_WIDTH - 5, 0.1, infoSlide[currentSlide].message);
		//jo_printf(1, 1, infoSlide[currentSlide].message);
		jo_sprite_change_sprite_scale(1.5);
		jo_sprite_draw3D(slides[infoSlide[currentSlide].slide], 0, -(jo_sprite_get_width(slides[0]) / 2), 500);
		
		if (buttonTimer <= 0)
		{
			if (jo_is_pad1_key_pressed(JO_MOUSE_START_BUTTON))
			{
				if (mode == 2)
				{
					//load_next_scene();
					jo_sprite_restore_sprite_scale();
					mode = -1;
					nextMode = 4;
				}
				else
				{
					mode = -1;
					nextMode = 5;
				}
			}
			else if (jo_is_pad1_key_pressed(JO_KEY_A))
			{
				if (currentSlide < endSlide)
				{
					play_audio(0);
					currentSlide += 1;
					buttonTimer = BUTTON_COOLER;
				}
				else 
				{
					if (mode == 2)
					{
						//load_next_scene();
						jo_sprite_restore_sprite_scale();
						mode = -1;
						nextMode = 4;
					}
					else
					{
						mode = -1;
						nextMode = 5;
					}
				}
			}
		}
		else
		{
			buttonTimer -= 1;
		}
	}
}

/*
** END OF FILE
*/
