#ifndef __SPEECHBUBBLE_H__
# define __SPEECHBUBBLE_H__

#include <jo/jo.h>

typedef struct {
	jo_pos2D position;
	int sprite;
	int origX;
	bool showing;
	char * message;
	int timer;
	int cooler;
} SpeechBubble;

extern SpeechBubble speechBubble;

void setup_speech_bubble(void);

#endif /* !__SPEECHBUBBLE_H__ */

