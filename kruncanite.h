#ifndef __KRUNCANITE_H__
# define __KRUNCANITE_H__

#include <jo/jo.h>
#include "brick.h"
#include "speechbubble.h"

typedef struct {
	jo_2d_object_attributes position;
	int currentState;
	int currentFrame;
	int timer;
	int cooler;
	float scale;
	
	jo_storyboard *spawnAnimation;
} Kruncanite;

extern Kruncanite kruncanites[4];
Kruncanite femaleKruncanites[2];
int kruncaniteSprites[4];
char * kruncaniteQuotes[10];
int currentQuote;
int endOfRandom;

void setup_kruncanites(void);
void spawn_new_kruncanite(int id);
void spawn_new_female_kruncanite(int id);
void animate_Kruncanite(int id);
//void kruncaniteLogic(int id);

#endif /* !__KRUNCANITE_H__ */

