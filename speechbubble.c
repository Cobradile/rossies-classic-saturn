#include "speechbubble.h"

SpeechBubble speechBubble;

void setup_speech_bubble(void)
{
	speechBubble.sprite = jo_sprite_add_tga("TEX", "SB.TGA", JO_COLOR_Transparent);
	speechBubble.timer = 0;
	speechBubble.cooler = 50;
	speechBubble.showing = false;
	speechBubble.position.y = -60;
}

void display_speech_bubble(int posX, char * newMessage)
{
	speechBubble.message = newMessage;
	speechBubble.origX = posX;
	if (posX < 0) 
	{
		speechBubble.position.x = posX + 60;
	}
	else
	{
		speechBubble.position.x = posX - 60;
	}

	speechBubble.showing = true;
	speechBubble.timer = 0;
}


