#ifndef __BRICK_H__
# define __BRICK_H__

#include <jo/jo.h>

typedef struct {
	jo_2d_object_attributes attributes;
	jo_pos2D target;
	jo_pos2D velocity;
	bool available;
	int sprite;
} Brick;

extern Brick brick;

void throwBrick(int posX, jo_pos2D newTarget);
void brickLogic(void);
void draw_brick(void);

#endif /* !__BRICK_H__ */

