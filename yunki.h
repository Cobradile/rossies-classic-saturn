#ifndef __YUNKI_H__
# define __YUNKI_H__

#include <jo/jo.h>

typedef struct {
	jo_pos2D position;
	jo_pos2D velocity;
	jo_pos2D target;
	jo_pos2D previousTarget;
	float scale;
	int health;
	int currentState;
	int currentFrame;
	int timer;
	int cooler;
	
	int frames[2];
} Yunki;

extern Yunki yunki;
int pass;

void yunkiSetup(void);
void yunkiSpawn(void);
void yunkiNewTarget(void);
void yunkiLogic(void);
void yunkiHit(void);

#endif /* !__YUNKI_H__ */


